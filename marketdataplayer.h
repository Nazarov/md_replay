#ifndef MD_REPLAY_MARKETDATAPLAYER_H
#define MD_REPLAY_MARKETDATAPLAYER_H

#include <string>
#include <unordered_map>
#include <memory>
#include <vector>
#include <functional>
#include "orderbook.h"

class MarketDataPlayer
{
public:
    explicit MarketDataPlayer(const char *filename, const char *symbol = "");
    void play();
private:
    std::string filename;
    std::string symbol;
    std::unordered_map<uint64_t, std::string> symbolByOrderId;
    std::unordered_map<std::string, OrderBook> orderBookBySymbol;
    std::unordered_map<std::string, size_t> bboSubscriptions;
    std::map<std::pair<std::string, uint64_t>, size_t> vwapSubscriptions;

    void createOrder(const std::string &command);
    void modifyOrder(const std::string &command);
    void cancelOrder(const std::string &command);
    void subscribeBbo(const std::string &command, bool subscribe);
    void subscribeVwap(const std::string &command, bool subscribe);
    void print(const std::string &command);
    void printFull(const std::string &command);
    std::vector<std::string> splitCommand(const std::string &command) const;
    bool processCommandArguments(const std::vector<std::string> &arguments,
                                 const std::function<void(size_t index, const std::string &)> &processing);
    OrderBook &getOrderBook(const std::string &symbol);
    size_t &getBboSubscription(const std::string &symbol);
    size_t &getVwapSubscription(const std::string &symbol, uint64_t quantity);
    bool symbolIsValid(const std::string &symbol);
    bool commandStartsWith(const std::string &command, const std::string &subString) const;
};

#endif //MD_REPLAY_MARKETDATAPLAYER_H
