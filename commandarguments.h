#ifndef MD_REPLAY_COMMANDARGUMENTS_H
#define MD_REPLAY_COMMANDARGUMENTS_H

enum class OrderAddArguments
{
    CommandName, OrderId, Symbol, Side, Quantity, Price, Size
};

enum class OrderModifyArguments
{
    CommandName, OrderId, Quantity, Price, Size
};

enum class OrderCancelArguments
{
    CommandName, OrderId, Size
};

enum class SubscribeBboArguments
{
    CommandName, Symbol, Size
};

enum class SubscribeVwapArguments
{
    CommandName, Symbol, Quantity, Size
};

enum class PrintArguments
{
    CommandName, Symbol, Size
};

enum class PrintFullArguments
{
    CommandName, Symbol, Size
};

#endif //MD_REPLAY_COMMANDARGUMENTS_H
