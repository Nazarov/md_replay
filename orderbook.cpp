#include "orderbook.h"

#include <iostream>
#include <sstream>
#include <algorithm>
#include <vector>
#include <iomanip>

OrderBook::OrderBook()
{
}

OrderBook::OrderBook(const std::string &symbol)
        : symbol(symbol)
{
}

void OrderBook::addOrder(std::unique_ptr<Order> &order)
{
    uint64_t id = order->id;
    orders[id] = std::move(order);
    addToPricesMap(orders[id]);
}

void OrderBook::modifyOrder(uint64_t id, uint64_t quantity, double price)
{
    try {
        const auto &order = orders.at(id);
        eraseFromPricesMap(getPricesMap(order->side), id);
        order->quantity = quantity;
        order->price = price;
        addToPricesMap(order);
    }
    catch (const std::out_of_range &ex) {
        std::cerr << "There is no order with such id(" << id << ")." << std::endl;
    }
}

void OrderBook::cancelOrder(uint64_t id)
{
    auto it = orders.find(id);
    if (it != orders.end()) {
        eraseFromPricesMap(getPricesMap(it->second->side), id);
        orders.erase(it);
    } else {
        std::cerr << "There is no order with such id(" << id << ")." << std::endl;
    }
}

void OrderBook::printBbo()
{
    std::string header = "Best bid/offer for " + symbol + ":\n" + tableHeader;
    std::cout << header << std::endl;
    if (!bbo.empty()) {
        std::cout << bbo << std::endl;
        return;
    }

    std::string bid = emptyLeftSide, ask = emptyRightSide, tempResult;
    double previousPrice = 0;
    uint64_t numberOfOrdersWithSamePrice = 0;
    uint64_t quantityOfOrdersWithSamePrice = 0;

    auto processOrder = [&](const std::unique_ptr<Order> &order, bool reverse = false) {
        if (previousPrice == order->price) {
            ++numberOfOrdersWithSamePrice;
            quantityOfOrdersWithSamePrice += order->quantity;
        } else {
            if (previousPrice != 0) {
                tempResult = combineHalfOfLevel(previousPrice, numberOfOrdersWithSamePrice,
                                                quantityOfOrdersWithSamePrice, reverse);
                return;
            }
            previousPrice = order->price;
            numberOfOrdersWithSamePrice = 1;
            quantityOfOrdersWithSamePrice = order->quantity;
        }
    };

    for (auto it = buyPrices.rbegin(); it != buyPrices.rend(); ++it) {
        processOrder(orders.at(it->second));
        if (!tempResult.empty()) {
            bid = tempResult;
            break;
        }
    }
    if (previousPrice != 0) {
        bid = combineHalfOfLevel(previousPrice, numberOfOrdersWithSamePrice, quantityOfOrdersWithSamePrice);
    }

    tempResult.clear();
    previousPrice = numberOfOrdersWithSamePrice = quantityOfOrdersWithSamePrice = 0;
    for (const auto &orderId:sellPrices) {
        processOrder(orders.at(orderId.second), true);
        if (!tempResult.empty()) {
            ask = tempResult;
            break;
        }
    }
    if (previousPrice != 0) {
        ask = combineHalfOfLevel(previousPrice, numberOfOrdersWithSamePrice, quantityOfOrdersWithSamePrice, true);
    }

    bbo = "|" + bid + "|" + ask + "|";
    std::cout << bbo << std::endl;
}

void OrderBook::printVwap(uint64_t quantity)
{
    uint64_t bidRemainder = quantity, askRemainder = quantity;
    double bidPrice = 0, askPrice = 0;

    auto processOrder = [&](const std::unique_ptr<Order> &order, uint64_t &remainder, double &price) {
        if (remainder <= order->quantity) {
            price += remainder * order->price;
            remainder = 0;
        } else {
            price += order->quantity * order->price;
            remainder -= order->quantity;
        }
    };

    for (auto it = buyPrices.rbegin(); it != buyPrices.rend(); ++it) {
        processOrder(orders.at(it->second), bidRemainder, bidPrice);
        if (!bidRemainder)
            break;
    }
    for (const auto &orderId:sellPrices) {
        processOrder(orders.at(orderId.second), askRemainder, askPrice);
        if (!askRemainder)
            break;
    }
    std::string bidResult, askResult;
    bidResult = bidRemainder ? "NIL" : getVwapWithAtLeastTwoDecimalPlaces(bidPrice / quantity);
    askResult = askRemainder ? "NIL" : getVwapWithAtLeastTwoDecimalPlaces(askPrice / quantity);

    std::cout << "VWAP(" << quantity << ") for " << symbol << ": <" << bidResult << ", " << askResult << ">"
              << std::endl;
}

void OrderBook::print()
{
    std::string header = "Order book for " + symbol + ":\n" + tableHeader;
    std::cout << header << std::endl;
    if (!printableBook.empty()) {
        std::cout << printableBook << std::endl;
        return;
    }

    std::vector<std::string> bid, ask;
    double previousPrice = 0;
    uint64_t numberOfOrdersWithSamePrice = 0;
    uint64_t quantityOfOrdersWithSamePrice = 0;

    auto processOrder = [&](std::vector<std::string> &side, const std::unique_ptr<Order> &order, bool reverse = false) {
        if (previousPrice == order->price) {
            ++numberOfOrdersWithSamePrice;
            quantityOfOrdersWithSamePrice += order->quantity;
        } else {
            if (previousPrice != 0) {
                side.emplace_back(
                        combineHalfOfLevel(previousPrice, numberOfOrdersWithSamePrice, quantityOfOrdersWithSamePrice,
                                           reverse));
            }
            previousPrice = order->price;
            numberOfOrdersWithSamePrice = 1;
            quantityOfOrdersWithSamePrice = order->quantity;
        }
    };

    for (auto it = buyPrices.rbegin(); it != buyPrices.rend(); ++it) {
        processOrder(bid, orders.at(it->second));
    }
    if (previousPrice != 0) {
        bid.emplace_back(combineHalfOfLevel(previousPrice, numberOfOrdersWithSamePrice, quantityOfOrdersWithSamePrice));
    }

    previousPrice = numberOfOrdersWithSamePrice = quantityOfOrdersWithSamePrice = 0;
    for (const auto &orderId:sellPrices) {
        processOrder(ask, orders.at(orderId.second), true);
    }
    if (previousPrice != 0) {
        ask.emplace_back(
                combineHalfOfLevel(previousPrice, numberOfOrdersWithSamePrice, quantityOfOrdersWithSamePrice, true));
    }

    size_t bookLength = std::max(bid.size(), ask.size());
    for (size_t i = 0; i < bookLength; ++i) {
        if (i < bid.size() && i < ask.size())
            printableBook.append("|" + bid.at(i) + "|" + ask.at(i) + "|\n");
        else if (i < bid.size())
            printableBook.append("|" + bid.at(i) + "|" + emptyRightSide + "|\n");
        else if (i < ask.size())
            printableBook.append("|" + emptyLeftSide + "|" + ask.at(i) + "|\n");
        if (i == 0)
            bbo = printableBook;
    }

    std::cout << printableBook << std::endl;
}

void OrderBook::printFull()
{
    for (auto it = buyPrices.rbegin(); it != buyPrices.rend(); ++it)
        printOrder(orders.at(it->second).get());
    for (const auto &orderId:sellPrices)
        printOrder(orders.at(orderId.second).get());
    std::cout << std::endl;
}

void OrderBook::enableBboSubscription()
{
    bboSubscriptionIsEnabled = true;
    printBbo();
}

void OrderBook::disableBboSubscription()
{
    bboSubscriptionIsEnabled = false;
}

void OrderBook::enableVwapSubscription(uint64_t quantity)
{
    vwapSubscriptions.insert(quantity);
    printVwap(quantity);
}

void OrderBook::disableVwapSubscription(uint64_t quantity)
{
    vwapSubscriptions.erase(quantity);
}

void OrderBook::printOrder(Order *order) const
{
    std::cout << "Order:"
              << " id=" << order->id
              << " symbol=" << order->symbol
              << " side=" << (order->side == OrderSide::Buy ? "Buy" : "Sell")
              << " quantity=" << order->quantity
              << " price=" << order->price
              << std::endl;
}

void OrderBook::addToPricesMap(const std::unique_ptr<Order> &order)
{
    getPricesMap(order->side).emplace(order->price, order->id);
    clearSavedStrings();
    updateSubscriptions();
}

void OrderBook::eraseFromPricesMap(std::multimap<double, uint64_t> &pricesMap, uint64_t id)
{
    auto range = pricesMap.equal_range(orders.at(id)->price);
    for (auto it = range.first; it != range.second; ++it) {
        if (it->second == id) {
            pricesMap.erase(it);
            break;
        }
    }
    clearSavedStrings();
    updateSubscriptions();
}

std::multimap<double, uint64_t> &OrderBook::getPricesMap(OrderSide side)
{
    return side == OrderSide::Buy ? buyPrices : sellPrices;
}

std::string OrderBook::combineHalfOfLevel(double price, uint64_t ordersNumber, uint64_t quantity, bool reverseOrder)
{
    std::stringstream stream;
    stream << std::fixed << std::setw(9) << std::setprecision(2) << price;
    std::string priceString = stream.str();

    stream.str(std::string());
    stream << std::setw(7) << ordersNumber;
    std::string orderString = stream.str();

    stream.str(std::string());
    stream << std::setw(8) << quantity;
    std::string quantityString = stream.str();

    return reverseOrder ?
           priceString + "|" + quantityString + "|" + orderString :
           orderString + "|" + quantityString + "|" + priceString;
}

void OrderBook::clearSavedStrings()
{
    printableBook.clear();
    bbo.clear();
}

void OrderBook::updateSubscriptions()
{
    if (bboSubscriptionIsEnabled)
        printBbo();
    for (const uint64_t &quantity:vwapSubscriptions)
        printVwap(quantity);
}

std::string OrderBook::getVwapWithAtLeastTwoDecimalPlaces(double vwap) const
{
    std::string result = std::to_string(vwap);
    std::string::size_type positionOfLastNotZeroSymbol = result.find_last_not_of('0');
    if (positionOfLastNotZeroSymbol == std::string::npos) {
        return result;
    }

    int offset = 1;
    std::string::size_type positionOfDotSymbol = result.find('.');
    if (positionOfLastNotZeroSymbol == positionOfDotSymbol)
        offset = 3;
    else if (positionOfLastNotZeroSymbol - positionOfDotSymbol == 1)
        offset = 2;
    result.erase(positionOfLastNotZeroSymbol + offset, std::string::npos);
    return result;
}
