#ifndef MD_REPLAY_ORDERBOOK_H
#define MD_REPLAY_ORDERBOOK_H

#include <string>
#include <unordered_map>
#include <memory>
#include <map>
#include <set>
#include "order.h"

class OrderBook
{
public:
    explicit OrderBook();
    explicit OrderBook(const std::string &symbol);

    void addOrder(std::unique_ptr<Order> &order);
    void modifyOrder(uint64_t id, uint64_t quantity, double price);
    void cancelOrder(uint64_t id);
    void printBbo();
    void printVwap(uint64_t quantity);
    void print();
    void printFull();
    void enableBboSubscription();
    void disableBboSubscription();
    void enableVwapSubscription(uint64_t quantity);
    void disableVwapSubscription(uint64_t quantity);
private:
    std::string symbol;
    std::string printableBook;
    std::string bbo;
    const std::string tableHeader = "|#orders| volume |   bid   |   ask   | volume |#orders|";
    const std::string emptyLeftSide = "       |        |         ";
    const std::string emptyRightSide = "         |        |       ";
    std::unordered_map<uint64_t, std::unique_ptr<Order>> orders;
    std::multimap<double, uint64_t> buyPrices;
    std::multimap<double, uint64_t> sellPrices;
    bool bboSubscriptionIsEnabled = false;
    std::set<uint64_t> vwapSubscriptions;

    void printOrder(Order *order) const;
    void addToPricesMap(const std::unique_ptr<Order> &order);
    void eraseFromPricesMap(std::multimap<double, uint64_t> &pricesMap, uint64_t id);
    std::multimap<double, uint64_t> &getPricesMap(OrderSide side);
    std::string combineHalfOfLevel(double price, uint64_t ordersNumber, uint64_t quantity, bool reverseOrder = false);
    void clearSavedStrings();
    void updateSubscriptions();
    std::string getVwapWithAtLeastTwoDecimalPlaces(double vwap) const;
};

#endif //MD_REPLAY_ORDERBOOK_H
