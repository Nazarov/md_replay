#ifndef MD_REPLAY_ORDER_H
#define MD_REPLAY_ORDER_H

#include <cstdint>
#include <string>

enum class OrderSide
{
    Buy, Sell
};

struct Order
{
    uint64_t id;
    std::string symbol;
    OrderSide side;
    uint64_t quantity;
    double price;
};

#endif //MD_REPLAY_ORDER_H
