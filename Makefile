CXX = g++
CXXFLAGS = -I.
BUILDDIR=build

_OBJ = main.o marketdataplayer.o orderbook.o
OBJ = $(patsubst %,$(BUILDDIR)/%,$(_OBJ))

$(shell mkdir -p $(BUILDDIR))

$(BUILDDIR)/md_replay: $(OBJ)
	$(CXX) $(CXXFLAGS) -o $@ $^

$(BUILDDIR)/main.o: main.cpp marketdataplayer.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<

$(BUILDDIR)/marketdataplayer.o: marketdataplayer.cpp commandarguments.h orderbook.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<

$(BUILDDIR)/orderbook.o: orderbook.cpp order.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<

.PHONY: clean

clean:
	rm -f $(BUILDDIR)/*