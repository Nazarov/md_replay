#include <iostream>
#include "marketdataplayer.h"

int main(int argc, char *argv[])
{
    if (argc == 1) {
        std::cerr << "Program requires file to work with." << std::endl;
    } else if (argc == 2 || argc == 3) {
        MarketDataPlayer marketDataPlayer(argv[1], (argc == 3 ? argv[2] : ""));
        marketDataPlayer.play();
    } else {
        std::cerr << "Invalid quantity of arguments." << std::endl;
    }
    return 0;
}
