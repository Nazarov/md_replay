#include "marketdataplayer.h"
#include "commandarguments.h"

#include <iostream>
#include <fstream>
#include <sstream>

MarketDataPlayer::MarketDataPlayer(const char *filename, const char *symbol)
        : filename(filename), symbol(symbol)
{
}

void MarketDataPlayer::play()
{
    std::ifstream file(filename);
    if (!file.is_open()) {
        std::cerr << "File \"" << filename << "\" can't be open." << std::endl;
        return;
    }
    std::string command;
    std::string addOrderCommand("ORDER ADD");
    std::string modifyOrderCommand("ORDER MODIFY");
    std::string cancelOrderCommand("ORDER CANCEL");
    std::string subscribeBboCommand("SUBSCRIBE BBO");
    std::string unsubscribeBboCommand("UNSUBSCRIBE BBO");
    std::string subscribeVwapCommand("SUBSCRIBE VWAP");
    std::string unsubscribeVwapCommand("UNSUBSCRIBE VWAP");
    std::string printCommand("PRINT");
    std::string fullPrintCommand("PRINT_FULL");
    while (std::getline(file, command)) {
        if (commandStartsWith(command, addOrderCommand))
            createOrder(command);
        else if (commandStartsWith(command, modifyOrderCommand))
            modifyOrder(command);
        else if (commandStartsWith(command, cancelOrderCommand))
            cancelOrder(command);
        else if (commandStartsWith(command, subscribeBboCommand))
            subscribeBbo(command, true);
        else if (commandStartsWith(command, unsubscribeBboCommand))
            subscribeBbo(command, false);
        else if (commandStartsWith(command, subscribeVwapCommand))
            subscribeVwap(command, true);
        else if (commandStartsWith(command, unsubscribeVwapCommand))
            subscribeVwap(command, false);
        else if (commandStartsWith(command, fullPrintCommand))
            printFull(command);
        else if (commandStartsWith(command, printCommand))
            print(command);
    }
}

void MarketDataPlayer::createOrder(const std::string &command)
{
    std::vector<std::string> values = splitCommand(command);
    if (values.size() != static_cast<size_t>(OrderAddArguments::Size)) {
        std::cerr << "Command \"" << command << "\" has wrong number of arguments" << std::endl;
        return;
    }
    std::unique_ptr<Order> order = std::make_unique<Order>();

    auto processing = [&](size_t index, const std::string &argument) {
        switch (static_cast<OrderAddArguments>(index)) {
            case OrderAddArguments::OrderId:
                order->id = std::stoull(argument);
                break;
            case OrderAddArguments::Symbol: {
                if (argument.empty())
                    throw std::runtime_error("Symbol is empty.");
                order->symbol = argument;
                break;
            }
            case OrderAddArguments::Side: {
                if (argument == "Buy")
                    order->side = OrderSide::Buy;
                else if (argument == "Sell")
                    order->side = OrderSide::Sell;
                else
                    throw std::runtime_error("Unsupported side.");
                break;
            }
            case OrderAddArguments::Quantity:
                order->quantity = std::stoull(argument);
                break;
            case OrderAddArguments::Price:
                order->price = std::stod(argument);
                break;
        }
    };

    if (processCommandArguments(values, processing) && symbolIsValid(order->symbol)) {
        if (symbolByOrderId.find(order->id) != symbolByOrderId.end()) {
            std::cerr << "Order with id " << order->id << " already exists." << std::endl;
            return;
        }

        symbolByOrderId.emplace(order->id, order->symbol);
        getOrderBook(order->symbol).addOrder(order);
    }
}

void MarketDataPlayer::modifyOrder(const std::string &command)
{
    std::vector<std::string> values = splitCommand(command);
    if (values.size() != static_cast<size_t>(OrderModifyArguments::Size)) {
        std::cerr << "Command \"" << command << "\" has wrong number of arguments" << std::endl;
        return;
    }
    uint64_t id = 0;
    uint64_t quantity = 0;
    double price = 0;
    auto processing = [&](size_t index, const std::string &argument) {
        switch (static_cast<OrderModifyArguments>(index)) {
            case OrderModifyArguments::OrderId:
                id = std::stoull(argument);
                break;
            case OrderModifyArguments::Quantity:
                quantity = std::stoull(argument);
                break;
            case OrderModifyArguments::Price:
                price = std::stod(argument);
                break;
        }
    };

    if (processCommandArguments(values, processing)) {
        try {
            const std::string &currentSymbol = symbolByOrderId.at(id);
            if (symbolIsValid(currentSymbol))
                getOrderBook(currentSymbol).modifyOrder(id, quantity, price);
        }
        catch (const std::out_of_range &ex) {
            std::cerr << "There is no order with id: " << id << std::endl;
        }
    }
}

void MarketDataPlayer::cancelOrder(const std::string &command)
{
    std::vector<std::string> values = splitCommand(command);
    if (values.size() != static_cast<size_t>(OrderCancelArguments::Size)) {
        std::cerr << "Command \"" << command << "\" has wrong number of arguments" << std::endl;
        return;
    }
    uint64_t id = 0;
    auto processing = [&](size_t index, const std::string &argument) {
        switch (static_cast<OrderCancelArguments >(index)) {
            case OrderCancelArguments::OrderId:
                id = std::stoull(argument);
                break;
        }
    };

    if (processCommandArguments(values, processing)) {
        try {
            const std::string &currentSymbol = symbolByOrderId.at(id);
            if (symbolIsValid(currentSymbol)) {
                getOrderBook(currentSymbol).cancelOrder(id);
                symbolByOrderId.erase(id);
            }
        }
        catch (const std::out_of_range &ex) {
            std::cerr << "There is no order with id: " << id << std::endl;
        }
    }
}

void MarketDataPlayer::subscribeBbo(const std::string &command, bool subscribe)
{
    std::vector<std::string> values = splitCommand(command);
    if (values.size() != static_cast<size_t>(SubscribeBboArguments::Size)) {
        std::cerr << "Command \"" << command << "\" has wrong number of arguments" << std::endl;
        return;
    }
    std::string symbol;
    auto processing = [&](size_t index, const std::string &argument) {
        switch (static_cast<SubscribeBboArguments >(index)) {
            case SubscribeBboArguments::Symbol:
                if (argument.empty())
                    throw std::runtime_error("Symbol is empty.");
                symbol = argument;
                break;
        }
    };

    if (processCommandArguments(values, processing) && symbolIsValid(symbol)) {
        if (subscribe) {
            ++getBboSubscription(symbol);
            getOrderBook(symbol).enableBboSubscription();
        } else {
            --getBboSubscription(symbol);
            if (!getBboSubscription(symbol))
                getOrderBook(symbol).disableBboSubscription();
        }
    }
}

void MarketDataPlayer::subscribeVwap(const std::string &command, bool subscribe)
{
    std::vector<std::string> values = splitCommand(command);
    if (values.size() != static_cast<size_t>(SubscribeVwapArguments::Size)) {
        std::cerr << "Command \"" << command << "\" has wrong number of arguments" << std::endl;
        return;
    }
    std::string symbol;
    uint64_t quantity = 0;
    auto processing = [&](size_t index, const std::string &argument) {
        switch (static_cast<SubscribeVwapArguments >(index)) {
            case SubscribeVwapArguments::Symbol:
                if (argument.empty())
                    throw std::runtime_error("Symbol is empty.");
                symbol = argument;
                break;
            case SubscribeVwapArguments::Quantity:
                quantity = std::stoull(argument);
                break;
        }
    };

    if (processCommandArguments(values, processing) && symbolIsValid(symbol)) {
        if (subscribe) {
            ++getVwapSubscription(symbol, quantity);
            getOrderBook(symbol).enableVwapSubscription(quantity);
        } else {
            --getVwapSubscription(symbol, quantity);
            if (!getVwapSubscription(symbol, quantity))
                getOrderBook(symbol).disableVwapSubscription(quantity);
        }
    }
}

void MarketDataPlayer::print(const std::string &command)
{
    std::vector<std::string> values = splitCommand(command);
    if (values.size() != static_cast<size_t>(PrintArguments::Size)) {
        std::cerr << "Command \"" << command << "\" has wrong number of arguments" << std::endl;
        return;
    }
    std::string symbol;
    auto processing = [&](size_t index, const std::string &argument) {
        switch (static_cast<PrintArguments >(index)) {
            case PrintArguments::Symbol:
                if (argument.empty())
                    throw std::runtime_error("Symbol is empty.");
                symbol = argument;
                break;
        }
    };

    if (processCommandArguments(values, processing) && symbolIsValid(symbol)) {
        getOrderBook(symbol).print();
    }
}

void MarketDataPlayer::printFull(const std::string &command)
{
    std::vector<std::string> values = splitCommand(command);
    if (values.size() != static_cast<size_t>(PrintFullArguments::Size)) {
        std::cerr << "Command \"" << command << "\" has wrong number of arguments" << std::endl;
        return;
    }
    std::string symbol;
    auto processing = [&](size_t index, const std::string &argument) {
        switch (static_cast<PrintFullArguments >(index)) {
            case PrintFullArguments::Symbol:
                if (argument.empty())
                    throw std::runtime_error("Symbol is empty.");
                symbol = argument;
                break;
        }
    };

    if (processCommandArguments(values, processing) && symbolIsValid(symbol)) {
        getOrderBook(symbol).printFull();
    }
}

std::vector<std::string> MarketDataPlayer::splitCommand(const std::string &command) const
{
    std::string value;
    std::vector<std::string> values;
    std::istringstream stream(command);
    while (getline(stream, value, ',')) {
        values.push_back(value);
    }
    return values;
}

bool MarketDataPlayer::processCommandArguments(const std::vector<std::string> &arguments,
                                               const std::function<void(size_t, const std::string &)> &precessing)
{
    for (size_t i = 0; i < arguments.size(); ++i) {
        try {
            precessing(i, arguments[i]);
        }
        catch (const std::invalid_argument &ex) {
            std::cerr << "Argument \"" << arguments[i] << "\" is invalid for conversion." << std::endl;
            return false;
        }
        catch (const std::out_of_range &ex) {
            std::cerr << "Argument \"" << arguments[i] << "\" is out of range." << std::endl;
            return false;
        }
        catch (const std::exception &ex) {
            std::cerr << ex.what() << std::endl;
            return false;
        }
        catch (...) {
            std::cerr << "Argument \"" << arguments[i] << "\" is invalid." << std::endl;
            return false;
        }
    }
    return true;
}

OrderBook &MarketDataPlayer::getOrderBook(const std::string &symbol)
{
    if (orderBookBySymbol.find(symbol) == orderBookBySymbol.end())
        orderBookBySymbol.emplace(symbol, OrderBook(symbol));
    return orderBookBySymbol[symbol];
}

size_t &MarketDataPlayer::getBboSubscription(const std::string &symbol)
{
    if (bboSubscriptions.find(symbol) == bboSubscriptions.end())
        bboSubscriptions.emplace(symbol, 0);
    return bboSubscriptions[symbol];
}

size_t &MarketDataPlayer::getVwapSubscription(const std::string &symbol, uint64_t quantity)
{
    auto pair = std::make_pair(symbol, quantity);
    if (vwapSubscriptions.find(pair) == vwapSubscriptions.end())
        vwapSubscriptions.emplace(pair, 0);
    return vwapSubscriptions[pair];
}

bool MarketDataPlayer::symbolIsValid(const std::string &symbol)
{
    return this->symbol.empty() || this->symbol == symbol;
}

bool MarketDataPlayer::commandStartsWith(const std::string &command, const std::string &subString) const
{
    return command.compare(0, subString.length(), subString) == 0;
}
